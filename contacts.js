/**
 * Created by Andrew Garcia on 3/8/2016
 * The contacts var holds the 15 objects that will be in the list
 * when the page is loaded
 */
var contacts = [
    {
        first : "Andy",
        last: "Garcia",
        email: "garcac01@students.ipfw.edu",
        phone: "260-341-4934"
    },
    {
        first: "Gizmo",
        last: "Garcia",
        email: "garcgm01@yahoo.com",
        phone: "260-555-1234"
    },
    {
        first: "Caitlin",
        last: "Greenwald",
        email: "cgree@gmail.com",
        phone: "260-555-4321"
    },
    {
        first: "Jimmy",
        last: "Smith",
        email: "jsmith@gmail.com",
        phone: "260-555-2345"
    },
    {
        first: "Kim",
        last: "Jackson",
        email: "kjack@yahoo.com",
        phone: "260-555-3456"
    },
    {
        first: "Peyton",
        last: "Manning",
        email: "pmanning@gmail.com",
        phone: "345-566-7892"
    },
    {
        first: "Tom",
        last: "Brady",
        email: "tbrady@yahoo.com",
        phone: "859-685-3843"
    },
    {
        first: "Albert",
        last: "Einstein",
        email: "aeinstein@aol.com",
        phone: "111-111-1111"
    },
    {
        first: "Steve",
        last: "Jobs",
        email: "sjobs@apple.com",
        phone: "222-222-2222"
    },
    {
        first: "Bill",
        last: "Gates",
        email: "bgates@msn.com",
        phone: "333-333-3333"
    },
    {
        first: "Satya",
        last: "Nadella",
        email: "snadella@msn.com",
        phone: "444-444-4444"
    },
    {
        first: "Matt",
        last: "Painter",
        email: "mpainter@purdue.edu",
        phone: "777-777-7777"
    },
    {
        first: "Gene",
        last: "Keady",
        email: "gkeady@purdue.edu",
        phone: "888-888-8888"
    },
    {
        first: "Barack",
        last: "Obama",
        email: "bobama@whitehouse.gov",
        phone: "999-999-9999"
    },
    {
        first: "Mark",
        last: "McGwire",
        email: "mmcgwire@gmail.com",
        phone: "988-293-3736"
    }
];

